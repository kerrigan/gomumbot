package utils

type PacketDataStream struct {
	data      []byte
	maxsize   uint32
	offset    uint32
	overshoot uint32
	ok        bool
}

func NewPDS(data []byte) *PacketDataStream {
	pds := new(PacketDataStream)
	pds.data = data
	pds.offset = 0
	pds.overshoot = 0
	pds.maxsize = uint32(len(data))
	pds.ok = true
	return pds
}

func (p *PacketDataStream) Size() uint32 {
	return p.offset
}

func (p *PacketDataStream) Capacity() uint32 {
	return p.maxsize
}

func (p *PacketDataStream) IsValid() bool {
	return p.ok
}

func (p *PacketDataStream) Left() uint32 {
	return p.maxsize - p.offset
}

func (p *PacketDataStream) Undersize() uint32 {
	return p.overshoot
}

func (p *PacketDataStream) Append(v uint64) {
	if p.offset < p.maxsize {
		p.data[p.offset] = byte(v)
		p.offset++
	} else {
		p.ok = false
		p.overshoot++
	}
}

func (p *PacketDataStream) Skip(length uint32) {
	if p.Left() >= length {
		p.offset += length
	} else {
		p.ok = false
	}
}

func (p *PacketDataStream) Next8() uint8 {
	if p.offset < p.maxsize {
		result := p.data[p.offset]
		p.offset++
		return result
	} else {
		p.ok = false
		return 0
	}
}

func (p *PacketDataStream) Rewind() {
	p.offset = 0
}

func (p *PacketDataStream) Truncate() {
	p.maxsize = p.offset
}

func (p *PacketDataStream) DataBlock(length uint32) []byte {
	if length <= p.Left() {
		result := p.data[p.offset : p.offset+length]
		p.offset += length
		return result
	} else {
		p.ok = false
		return []byte{}
	}
}
