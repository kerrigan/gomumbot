package utils

import (
	"encoding/pem"
	"errors"
	"fmt"
	"log"
	"math/big"
	"os"
	"time"

	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"

	proto "github.com/golang/protobuf/proto"
)

// utils
func ParsePrefix(buffer []byte) (msgType uint32, length uint32) {
	//buf := bytes.NewReader(buffer[:2])
	//var messageType int16
	//var messageLength int32

	/*
		err := binary.Read(buf, binary.BigEndian, &messageType)

		if err != nil {
			panic("Error in mumble prefix parsing")
		}

		buf = bytes.NewReader(buffer[2:])

		fmt.Println("Length of length", len(buffer[2:]))

		err = binary.Read(buf, binary.BigEndian, &messageLength)

		if err != nil {
			panic("Error in mumble prefix parsing")
		}
	*/
	msgType = (uint32(buffer[0]) << 8) | uint32(buffer[1])

	length = (uint32(buffer[2]) << 24) |
		(uint32(buffer[3]) << 16) |
		(uint32(buffer[4]) << 8) |
		uint32(buffer[5])

	return
	//return Bytes2int(buffer[:2]), Bytes2int(buffer[2:]) //uint32(messageType), uint32(messageLength)
}

func buildPrefix(packetType uint32, packetLength uint32) []byte {
	return []byte{
		byte(packetType >> 8),
		byte(packetType & 0xff),
		byte(packetLength >> 24),
		byte(packetLength >> 16),
		byte(packetLength >> 8),
		byte(packetLength & 0xff),
	}
}

func Bytes2int(buffer []byte) uint32 {
	var a uint32 = 0

	l := len(buffer)
	for i, b := range buffer {
		shift := uint32((l - i - 1) * 8)

		a |= uint32(b) << shift
	}
	return a
}

/*
func Bytes2int(buffer []uint8) uint32 {
	var a uint32 = 0
	l := len(buffer)
	for i := l; i >= 0; i-- {
		shift := uint32(8 * (l - i - 1))

		a |= uint32(buffer[i] << shift)
	}

	return a
}
*/

/*
func Bytes2int(buffer []uint8) (ret uint32) {
	buf := bytes.NewBuffer(buffer)
	binary.Read(buf, binary.BigEndian, &ret)
	return
}
*/

func bytes2int64(buffer []byte) uint64 {
	var a uint64

	l := len(buffer)
	for i, b := range buffer {
		//shift := uint64((l - i - 1) * 8)
		shift := uint64((l - i - 1) * 8)

		a |= uint64(b) << shift
	}
	return a
}

func PackMessage(packetType int, msg proto.Message) []byte {
	buffer, _ := proto.Marshal(msg)

	return append(
		buildPrefix(uint32(packetType), uint32(len(buffer))),
		buffer...)
}

func Byte2string(word byte) string {
	result := ""
	var mask byte = 1
	for i := 0; i < 8; i++ {
		result = string(int(word&mask)) + result
		fmt.Println(word & mask)
		mask = mask << 1
	}
	return result
}

func makeWithNulls(length int) []byte {
	result := make([]byte, length)
	for i := 0; i < length; i++ {
		result[i] = 0x0
	}
	return result
}

/*
func DecodeVarint(head []byte) (result int64, shift int64, err error) { //returns varint and shift
	header := head[0]

	// const (
	//     ONE_BYTE_MASK = 0x80,
	//     TWO_BYTE_MASK = 0x80,
	//     THREE_BYTE_MASK = 0xc0,
	// )
	// 0x80 - 0b10000000
	// 0xc0 - 0b11000000
	// 0xe0 - 0b11100000

	var varint []byte
	//	var shift uint8

	if header&0x80 == 0x0 { //  - 1 byte
		//varint = append(makeWithNulls(7), header)
		shift = 1
		result = uint64(header & 0x7f)
		return
	} else if header&0x80 == 0x80 { // - 2 bytes
		tail := []byte{header & 0x7f, head[1]} // 0x7f - 0x01111111
		varint = append(makeWithNulls(6), tail...)
		shift = 2
	} else if header&0xc0 == 0xc0 { // - 3 bytes
		tail := []byte{header & 0x3f, head[1], head[2]} // 0x3f - 0x00111111
		varint = append(makeWithNulls(5), tail...)
		shift = 3
	} else if header&0xe0 == 0xe0 { // - 4 bytes
		tail := []byte{header & 0x1f, head[1], head[2], head[3]} // 0x1f 0x00011111
		varint = append(makeWithNulls(4), tail...)
		shift = 4
	} else if header&0xf0 == 0xf0 { // 32bit, next 4 bytes
		varint = head[1:5]
		shift = 5
	} else if header&0xf4 == 0xf4 { // 64bit, next 8 bytes
		varint = head[1:9]
		shift = 9
	} else {
		panic("Wrong varint")
	}
	//maybe never or later
	buf := bytes.NewReader(varint)

	//var result uint64

	err = binary.Read(buf, binary.BigEndian, &result)

	if err != nil {
		fmt.Println(err)
		panic("Error in decoding varint")
	}

	return
}
*/

//*
func DecodeVarint(buf []byte) (result int64, shift uint8, err error) {
	shift = 0
	result = 0

	if len(buf) <= 0 {
		err = errors.New("Wrong varint")
		return
	}

	first := buf[0]

	isNegative := false

	if first&0xfc == 0xf8 {
		isNegative = true
		shift++

		if len(buf) < 2 {
			err = errors.New("Too short negative varint")
			return
		}

		buf = buf[1:]

		first = buf[0]
	}

	if first&0x80 == 0x0 {
		result = int64(buf[0])
		shift++
	} else if first&0xfc == 0xfc {
		result = int64(buf[0])
		result = result & 0x3
		isNegative = true
		shift++
	} else if first&0xc0 == 0x80 {
		if len(buf) < 2 {
			err = errors.New("Too short 2 bytes varint")
			return
		}
		result = int64(bytes2int64(buf[:2]))
		result = result & 0x3fff
		shift += 2
	} else if first&0xe0 == 0xc0 {
		if len(buf) < 3 {
			err = errors.New("Too short 3 bytes varint")
			return
		}
		result = int64(bytes2int64(buf[:3]))
		result = result & 0x1f
		shift += 3
	} else if first&0xf0 == 0xe0 {
		if len(buf) < 4 {
			err = errors.New("Too short 4 bytes varint")
			return
		}
		result = int64(bytes2int64(buf[:4]))
		shift += 4
	} else if first&0xfc == 0xf0 {
		if len(buf) < 5 {
			err = errors.New("Too short 5 bytes varint")
			return
		}
		result = int64(bytes2int64(buf[1:5]))
		shift += 5
	} else if first&0xfc == 0xf4 {
		if len(buf) < 9 {
			err = errors.New("Too short 9 bytes varint")
			return
		}
		result = int64(bytes2int64(buf[1:9]))
		shift += 9
	}

	if isNegative {
		result = -result
	}

	return

}

//*/

func IsFileExists(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

func CheckKeys() {
	keyPath := "certs"
	keyFilename := keyPath + "/client.key"
	certFilename := keyPath + "/client.pem"

	if IsFileExists(keyPath) && IsFileExists(keyFilename) && IsFileExists(certFilename) {
		return
	}
	os.Mkdir("certs", 0755)

	fpriv, err := os.Create(keyFilename)
	if err != nil {
		log.Fatal(err)
	}

	fpub, err := os.Create(certFilename)

	if err != nil {
		log.Fatal(err)
	}

	size := 2048
	priv, err := rsa.GenerateKey(rand.Reader, size)

	if err != nil {
		log.Fatal(err)
	}

	template := &x509.Certificate{
		IsCA:                  true,
		BasicConstraintsValid: true,
		SubjectKeyId:          []byte{1, 2, 3},
		SerialNumber:          big.NewInt(1234),
		Subject: pkix.Name{
			Country:      []string{"Rochland"},
			Organization: []string{"Mother Nature"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().AddDate(30, 5, 5),
		// see http://golang.org/pkg/crypto/x509/#KeyUsage
		ExtKeyUsage: []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:    x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
	}

	var parent = template

	cert, err := x509.CreateCertificate(rand.Reader, template, parent, &priv.PublicKey, priv)

	if err != nil {
		log.Fatal(err)
	}

	pem.Encode(fpub, &pem.Block{Type: "CERTIFICATE", Bytes: cert})

	pem.Encode(fpriv,
		&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(priv),
		},
	)

	fmt.Println("New keypair created")

}
