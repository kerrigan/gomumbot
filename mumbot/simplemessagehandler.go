package mumbot

import (
	"fmt"
)

type SimpleMessageHandler struct {
}

func (p *SimpleMessageHandler) ProcessMessage(bot *MumbleBot, user *User, private bool, message string) {
	if private {
		fmt.Print("Private: ")
	}
	fmt.Println(user.Nick, "wrote:", message)
	//bot.WriteMessage("Получено сообщение от: " + user.Nick)
}
