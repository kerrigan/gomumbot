package mumbot

type ChannelListHandler interface {
	ChannelListChanged(p *MumbleBot, channels map[uint32]*Channel)
}

type ConnectedHandler interface {
	Connected(bot *MumbleBot)
}

type UserStateHandler interface {
	UserStateChanged(p *MumbleBot, users map[uint32]*User)
}

type VoiceHandler interface {
	ProcessVoice(session int64, sequence int64, last bool, data []byte)
}

type MessageHandler interface {
	ProcessMessage(bot *MumbleBot, user *User, private bool, message string)
}
