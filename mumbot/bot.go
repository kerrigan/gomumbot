package mumbot

import (
	"bufio"
	"crypto/tls"

	"fmt"

	"io"
	"net"
	//	"os"
	"strconv"
	"time"

	MumbleProto "bitbucket.org/kerrigan/gomumbot/mumbleproto"
	utils "bitbucket.org/kerrigan/gomumbot/mumbot/utils"
	proto "github.com/golang/protobuf/proto"
)

import "C"

const (
	//Message types
	version             = 0
	udptunnel           = 1
	authenticate        = 2
	ping                = 3
	reject              = 4
	serversync          = 5
	channelremove       = 6
	channelstate        = 7
	userremove          = 8
	userstate           = 9
	banlist             = 10
	textmessage         = 11
	permissiondenied    = 12
	acl                 = 13
	queryusers          = 14
	cryptsetup          = 15
	contextactionmodify = 16
	contextaction       = 17
	userlist            = 18
	voicetarget         = 19
	permissionquery     = 20
	codecversion        = 21
	userstats           = 22
	requestblob         = 23
	serverconfig        = 24
	suggestconfig       = 25

	//Permissions
	None            = 0x0
	Write           = 0x1
	Traverse        = 0x2
	Enter           = 0x4
	Speak           = 0x8
	MuteDeafen      = 0x10
	Move            = 0x20
	MakeChannel     = 0x40
	LinkChannel     = 0x80
	Whisper         = 0x100
	TextMessage     = 0x200
	MakeTempChannel = 0x400

	// Root channel only
	Kick         = 0x10000
	Ban          = 0x20000
	Register     = 0x40000
	SelfRegister = 0x80000

	Cached = 0x8000000
	All    = 0xf07ff
)

// var publishChannel = make(chan byte)

//var encoderInput = make(chan []byte)

//var shoutInput chan []byte = make(chan []byte, 4096)

//var logStorage = utils.NewLogStorage()

//var debugBus = make(chan []byte)

type MumbleBot struct {
	nick        string
	ip          string
	port        int
	conn        net.Conn
	running     bool
	SampleCount int
	users       map[uint32]*User
	channels    map[uint32]*Channel
	userID      uint32

	voiceQueue chan []byte
	voiceCtrl  chan int

	//voiceProcessor *utils.VoiceProcessor

	connectedHandler   *ConnectedHandler
	voiceHandler       *VoiceHandler
	messageHandler     *MessageHandler
	userStateHandler   *UserStateHandler
	channelListHandler *ChannelListHandler
}

type User struct {
	ID        uint32
	Nick      string
	ChannelID uint32
}

type Channel struct {
	ID       uint32
	Name     string
	ParentID uint32
}

func NewMumbleBot(ip string, port int, nick string) *MumbleBot {
	bot := &MumbleBot{
		nick:        nick,
		ip:          ip,
		port:        port,
		running:     true,
		SampleCount: 0,
		users:       make(map[uint32]*User),
		channels:    make(map[uint32]*Channel),
		//voiceProcessor: utils.NewVoiceProcessor(),
	}
	return bot
}

func (p *MumbleBot) SetConnectedHandler(handler ConnectedHandler) {
	p.connectedHandler = &handler
}

func (p *MumbleBot) SetVoiceHandler(handler VoiceHandler) {
	p.voiceHandler = &handler
}

func (p *MumbleBot) SetMessageHandler(handler MessageHandler) {
	p.messageHandler = &handler
}

func (p *MumbleBot) SetUserStateHandler(handler UserStateHandler) {
	p.userStateHandler = &handler
}

func (p *MumbleBot) SetChannelListHandler(handler ChannelListHandler) {
	p.channelListHandler = &handler
}

func (p *MumbleBot) Stop() {
	p.running = false
	p.voiceCtrl <- 1
}

func (p MumbleBot) GetChannels() map[uint32]*Channel {
	return p.channels
}

func (p MumbleBot) GetUsers() map[uint32]*User {
	return p.users
}

func (p MumbleBot) GetUserID() uint32 {
	return p.userID
}

/*
func (p *MumbleBot) processVoice() {
	for {
		select {
		case buffer := <-p.voiceQueue:
			voice, sequence, session, last := p.voiceProcessor.ProcessVoice(buffer)

			if len(voice) > 0 {
				(*p.voiceHandler).ProcessVoice(session, sequence, last, voice)
			}
		case <-p.voiceCtrl:
			return
		}
	}
}
*/

// Run - run mumble bot
func (p *MumbleBot) Run() {
	utils.CheckKeys()

	cert, err := tls.LoadX509KeyPair("certs/client.pem", "certs/client.key")
	if err != nil {
		fmt.Println(err)
		panic("server: loadkeys:")
	}
	config := tls.Config{Certificates: []tls.Certificate{cert}, InsecureSkipVerify: true}

	//server := "mumble-ru.cleanvoice.ru:64040"
	server := p.ip + ":" + strconv.Itoa(p.port)

	fmt.Println(server)

	conn, err := tls.Dial("tcp", server, &config)

	if err != nil {
		fmt.Println("Connection error")
		return
	}

	if p.connectedHandler != nil {
		(*p.connectedHandler).Connected(p)
	}

	p.conn = conn

	defer conn.Close()

	go p.authThread()

	p.readThread()
}

func (p *MumbleBot) authThread() {
	verPacketProto := makeVersion()

	authPacketProto := makeAuth(p.nick)

	packedVerPacket := utils.PackMessage(version, &verPacketProto)
	packedAuthPacket := utils.PackMessage(authenticate, &authPacketProto)

	p.conn.Write(append(packedVerPacket, packedAuthPacket...))

	go p.pingThread()
}

func (p *MumbleBot) readThread() {
	//runtime.LockOSThread()

	//ioprio.SetIoPrio(ioprio.Process, 0, ioprio.RealTime, ioprio.BestEffortNr)

	prefixBuffer := make([]byte, 6)

	reader := bufio.NewReader(p.conn)

	for p.running {
		readlen, err := io.ReadFull(reader, prefixBuffer)

		//debugBus <- prefixBuffer

		if err != nil {
			fmt.Println(err)
			panic("Read error")
		}

		if readlen != 6 {
			panic("Too short prefix")
		}

		//fmt.Println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
		/*
		   fmt.Println("Prefix  buffer", hex.Dump(prefixBuffer))
		*/
		packetType, packetLength := utils.ParsePrefix(prefixBuffer)

		packetBuffer := make([]byte, packetLength)

		//conn.Read(packetBuffer)// /bufio.NewReader(conn)
		readlen, err = io.ReadFull(reader, packetBuffer)

		//debugBus <- packetBuffer

		if err != nil {
			panic("Mumble read error")
		}

		if uint32(readlen) != packetLength {
			panic("Mumble data packet size error")
		}

		p.processPacket(p.conn, packetType, packetBuffer)

		// fmt.Println("Packet type ", packetType)
		//fmt.Println("Bytes read ", packetLength)
	}

	//runtime.UnlockOSThread()
}

func writeThread(conn io.Writer, buffer []byte) {
	conn.Write(buffer)
}

func (p *MumbleBot) pingThread() {
	pingProto := makePing()
	ticker := time.NewTicker(time.Millisecond * 1000 * 10)
	for _ = range ticker.C {
		p.conn.Write(utils.PackMessage(ping, &pingProto))
		// fmt.Println("Ping sent", t)
	}
}

func (p *MumbleBot) processPacket(conn net.Conn, packetType uint32, buffer []byte) {
	//BUFFER_SIZE := 1024
	switch packetType {
	case version:
		versionPacketProto := new(MumbleProto.Version)
		proto.Unmarshal(buffer, versionPacketProto)
		fmt.Println(
			"OS:", versionPacketProto.GetOs(),
			"version:", versionPacketProto.GetVersion(),
		)
	case udptunnel:
		/*
			if p.voiceHandler != nil {
				//TODO: ЗАДЕРЖКА возможно тут стоит складывать звук в очередь и дергать хендлер когда всё раздекодируется

				if p.voiceHandler != nil {
					if p.voiceQueue == nil {
						p.voiceQueue = make(chan []byte, 32768)
						p.voiceCtrl = make(chan int)
						go p.processVoice()
					}
					p.voiceQueue <- buffer
				}
			}
		*/
	case textmessage:
		//Process text message
		if p.messageHandler != nil {
			textPacketProto := new(MumbleProto.TextMessage)

			proto.Unmarshal(buffer, textPacketProto)

			user := p.users[textPacketProto.GetActor()]
			if user.Nick == "" {
				return
			}
			message := textPacketProto.GetMessage()

			private := false
			if len(textPacketProto.ChannelId) == 0 && len(textPacketProto.Session) > 0 {
				private = true
			} else if len(textPacketProto.ChannelId) > 0 && len(textPacketProto.Session) == 0 {
				private = false
			}

			if p.messageHandler != nil {
				(*p.messageHandler).ProcessMessage(p, user, private, message)
			}
		}
	case userstate:
		// Список пользователей
		userPacketProto := new(MumbleProto.UserState)
		proto.Unmarshal(buffer, userPacketProto)

		userID := userPacketProto.GetSession()
		if user, ok := p.users[userID]; ok {
			//Если пользователь есть - обновляем существующие поля
			if userPacketProto.Name != nil {
				user.Nick = userPacketProto.GetName()
			}

			if userPacketProto.ChannelId != nil {
				user.ChannelID = userPacketProto.GetChannelId()
			}

			p.users[userID] = user

		} else {
			//Если не существует - создаем нового
			nick := userPacketProto.GetName()
			channelID := userPacketProto.GetChannelId()
			p.users[userPacketProto.GetSession()] = &User{
				ID:        userID,
				Nick:      nick,
				ChannelID: channelID,
			}

			if nick == p.nick {
				p.userID = userID
			}
		}
		if p.userStateHandler != nil {
			(*p.userStateHandler).UserStateChanged(p, p.users)
		}

	case userremove:
		userPacketProto := new(MumbleProto.UserRemove)
		proto.Unmarshal(buffer, userPacketProto)
		delete(p.users, userPacketProto.GetSession())
		if p.userStateHandler != nil {
			(*p.userStateHandler).UserStateChanged(p, p.users)
		}
	case cryptsetup:
		cryptSetupProto := new(MumbleProto.CryptSetup)
		proto.Unmarshal(buffer, cryptSetupProto)
		fmt.Println("CryptSetup")
		fmt.Println("Key", cryptSetupProto.GetKey())
		fmt.Println("Client", cryptSetupProto.GetClientNonce())
		fmt.Println("Server", cryptSetupProto.GetServerNonce())
	case channelstate:
		channelStateProto := new(MumbleProto.ChannelState)
		proto.Unmarshal(buffer, channelStateProto)

		channelID := channelStateProto.GetChannelId()
		p.channels[channelID] = &Channel{
			ID:       channelID,
			Name:     channelStateProto.GetName(),
			ParentID: channelStateProto.GetParent(),
		}

		if p.channelListHandler != nil {
			(*p.channelListHandler).ChannelListChanged(p, p.channels)
		}
		fmt.Println("Channel", channelStateProto.GetChannelId(), channelStateProto.GetName(), channelStateProto.GetParent())
	case channelremove:
		channelRemoveProto := new(MumbleProto.ChannelRemove)
		proto.Unmarshal(buffer, channelRemoveProto)
		delete(p.channels, channelRemoveProto.GetChannelId())
		if p.channelListHandler != nil {
			(*p.channelListHandler).ChannelListChanged(p, p.channels)
		}
		fmt.Println("Channel", channelRemoveProto.GetChannelId(), "removed")
	case permissionquery:
		permissionQueryProto := new(MumbleProto.PermissionQuery)
		proto.Unmarshal(buffer, permissionQueryProto)
		fmt.Println("PQ:", permissionQueryProto)
	case requestblob:
		requestBlobProto := new(MumbleProto.RequestBlob)
		proto.Unmarshal(buffer, requestBlobProto)
		fmt.Println("RB:", requestBlobProto)
	case ping:
	default:
		fmt.Println("Packet type", packetType)
	}
}

func (p *MumbleBot) WriteMessage(text string) {
	textPacketProto := new(MumbleProto.TextMessage)
	textPacketProto.Message = &text
	selfChannelID := p.users[p.userID].ChannelID
	textPacketProto.ChannelId = []uint32{selfChannelID}
	p.conn.Write(utils.PackMessage(textmessage, textPacketProto))
}

func (p *MumbleBot) WritePrivateMessage(userID uint32, text string) {
	textPacketProto := new(MumbleProto.TextMessage)
	textPacketProto.Message = &text
	textPacketProto.Session = []uint32{userID}
	p.conn.Write(utils.PackMessage(textmessage, textPacketProto))
}

func (p MumbleBot) GoToChannel(channelID uint32) {
	userStateProto := new(MumbleProto.UserState)
	userStateProto.Session = &p.userID
	userStateProto.UserId = &p.userID
	userStateProto.ChannelId = &channelID
	p.conn.Write(utils.PackMessage(userstate, userStateProto))
}

func (p MumbleBot) MoveToChannel(channelID, userID uint32) {
	userStateProto := new(MumbleProto.UserState)
	userStateProto.Session = &userID
	userStateProto.ChannelId = &channelID
	p.conn.Write(utils.PackMessage(userstate, userStateProto))
}

func (p MumbleBot) KickUser(userID uint32, reason string) {
	userRemoveProto := new(MumbleProto.UserRemove)
	userRemoveProto.Session = &userID
	userRemoveProto.Reason = &reason
	p.conn.Write(utils.PackMessage(userremove, userRemoveProto))
}

func (p MumbleBot) BanUser(userID uint32, reason string) {
	userRemoveProto := new(MumbleProto.UserRemove)
	userRemoveProto.Session = &userID
	userRemoveProto.Reason = &reason
	isBan := true
	userRemoveProto.Ban = &isBan
	p.conn.Write(utils.PackMessage(userremove, userRemoveProto))
}

func (p MumbleBot) MuteUser(userID uint32, mute bool) {
	userStateProto := new(MumbleProto.UserState)
	userStateProto.Session = &userID
	userStateProto.Mute = &mute
	p.conn.Write(utils.PackMessage(userstate, userStateProto))
}

func (p MumbleBot) DeafUser(userID uint32, deaf bool) {
	userStateProto := new(MumbleProto.UserState)
	userStateProto.Session = &userID
	userStateProto.Deaf = &deaf
	p.conn.Write(utils.PackMessage(userstate, userStateProto))
}

func (p *MumbleBot) CreateChannel(parentID uint32, name string) {
	createChannelProto := new(MumbleProto.ChannelState)
	createChannelProto.Parent = &parentID
	createChannelProto.Name = &name
	p.conn.Write(utils.PackMessage(channelstate, createChannelProto))
}

func (p *MumbleBot) RemoveChannel(channelID uint32) {
	removeChannelProto := new(MumbleProto.ChannelState)
	removeChannelProto.ChannelId = &channelID
	p.conn.Write(utils.PackMessage(channelremove, removeChannelProto))
}

func (p *MumbleBot) UpdateChannel(channelID uint32, name string) {
	updateChannelProto := new(MumbleProto.ChannelState)
	updateChannelProto.ChannelId = &channelID
	updateChannelProto.Name = &name
	p.conn.Write(utils.PackMessage(channelstate, updateChannelProto))
}

// Mumble packets
func makeAuth(username string) MumbleProto.Authenticate {
	authPacket := MumbleProto.Authenticate{Username: &username}
	enableOpus := true
	authPacket.Opus = &enableOpus
	// authPacket.Tokens = []string{}
	//password := "123"
	//authPacket.Password = &password
	//login := "SuperUser"
	//authPacket.Username = &login
	return authPacket
}

func makeVersion() MumbleProto.Version {
	verMsg := MumbleProto.Version{}
	release := "1.2.8"
	verMsg.Release = &release

	var version = utils.Bytes2int([]byte{0, 1, 2, 8}) //66048
	verMsg.Version = &version

	os := "botOS"
	verMsg.Os = &os

	osVersion := "gomumbot0.0.1"

	verMsg.OsVersion = &osVersion

	return verMsg
}

func makePing() MumbleProto.Ping {
	//TODO: add various data
	return MumbleProto.Ping{}
}
